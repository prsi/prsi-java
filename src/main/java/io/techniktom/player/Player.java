package io.techniktom.player;

import io.techniktom.card.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Player {
    private final List<Card> deck;
    private final String name;
    private Integer deckSize;

    public Player(String name) {
        this.name = name;

        this.deck = new ArrayList<>();
        this.deckSize = 0;
    }

    public List<Card> getDeck() {
        return Collections.unmodifiableList(deck);
    }

    public void clearDeck() {
        deck.clear();
    }

    public boolean isPlaying() {
        return deckSize > 0;
    }

    public void addCardToDeck(Card card) {
        deck.add(card);
        deckSize++;
    }

    public void addCardsToDeck(List<Card> cards) {
        deck.addAll(cards);
        deckSize += cards.size();
    }

    public void removeCardFromDeck(Card card) {
        deck.remove(card);
        deckSize--;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("Player{name: %s}", name);
    }
}

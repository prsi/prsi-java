package io.techniktom.interfaces;

import io.techniktom.card.Card;
import io.techniktom.card.CardColor;
import io.techniktom.player.Player;
import io.techniktom.player.Players;

import java.util.Optional;

public interface GameInterface {
    void gameStart();

    Optional<Card> letPlayerSelectCard(Player player, Card currentCard, CardColor colorOnChanger, Integer overChargeCount);

    void showFirstCard(Card currentCard);

    void placeCard(Card selectedCard);

    void addCardToDeck(Player player, Card retrievedCard);

    CardColor letPlayerSelectColor();

    void playerRetrievedOverChargedCards(Player player, Integer count);

    void gameOver(Players players, Integer round);

    void currentPlayer(Player player);
}

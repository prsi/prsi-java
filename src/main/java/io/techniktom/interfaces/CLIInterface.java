package io.techniktom.interfaces;

import io.techniktom.card.Card;
import io.techniktom.card.CardColor;
import io.techniktom.card.CardType;
import io.techniktom.player.Player;
import io.techniktom.player.Players;
import io.techniktom.translations.Translation;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

public class CLIInterface implements GameInterface {
    private final BufferedReader cliReader;
    private final PrintWriter cliWriter;
    private final Translation translation;

    public CLIInterface(Translation translation) throws UnsupportedEncodingException {
        cliReader = new BufferedReader(new InputStreamReader(System.in, recogniseCharsetBySystem()));
        cliWriter = new PrintWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8), true);
        this.translation = translation;
    }

    private String recogniseCharsetBySystem() {
        return "windows-1250";
    }

    private void clearConsole() {
//        cliWriter.print("\033[H\033[2J");
//        cliWriter.flush();

        cliWriter.println("-------------------------------------------------------------------------");
        cliWriter.flush();
    }

    @Override
    public void gameStart() {
        clearConsole();

        cliWriter.println(translation.getTranslation("game_start"));
        cliWriter.println();
        cliWriter.print(translation.getTranslation("prompt_for_continue"));
        cliWriter.flush();

        try {
            cliReader.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Card> letPlayerSelectCard(Player player, Card card, CardColor colorOnChanger, Integer overChargeCount) {
        var isSpecial = card.isSpecial() && overChargeCount != null;

        cliWriter.println();
        if(card.isSuperSpecial() && colorOnChanger != null) {
            cliWriter.print(translation.getTranslation("current_color"));
            cliWriter.println(":");
            cliWriter.println(translation.getCardColorName(colorOnChanger));
        } else {
            cliWriter.print(translation.getTranslation("current_card"));
            cliWriter.println(":");
            cliWriter.println(translation.getCardName(card));
        }
        cliWriter.println();

        var cardsForSelect = new ArrayList<Card>();
        var remainingCards = new ArrayList<Card>();

        if(card.isSuperSpecial()) {
            for (Card card1 : player.getDeck()) {
                if(Objects.equals(card1.getColor(), colorOnChanger)) {
                    cardsForSelect.add(card1);
                } else {
                    remainingCards.add(card1);
                }
            }
        } else if(isSpecial) {
            for (Card card1 : player.getDeck()) {
                if(Objects.equals(card1.getType(), card.getType())) {
                    cardsForSelect.add(card1);
                } else {
                    remainingCards.add(card1);
                }
            }
        } else {
            for (Card card1 : player.getDeck()) {
                if(card1.isSuperSpecial() || card.compare(card1)) {
                    cardsForSelect.add(card1);
                } else {
                    remainingCards.add(card1);
                }
            }
        }

        if(cardsForSelect.size() > 0) {
            cliWriter.print(translation.getTranslation("cards_for_select"));
            cliWriter.println(":");

            for (int i = 0; i < cardsForSelect.size(); i++) {
                cliWriter.printf("%d) %s \n", i, translation.getCardName(cardsForSelect.get(i)));
            }

            cliWriter.println();


            if(remainingCards.size() > 0) {
                cliWriter.print(translation.getTranslation("remaining_cards"));
                cliWriter.println(":");

                for (Card remainingCard : remainingCards) {
                    cliWriter.printf("\t%s\n", translation.getCardName(remainingCard));
                }
            }

            cliWriter.println();

            cliWriter.println(translation.getTranslation("select_card"));
            cliWriter.print(": ");
            cliWriter.flush();

            try {
                var response = cliReader.readLine();

                return Optional.of(cardsForSelect.get(Integer.parseInt(response)));
            } catch (IOException | NumberFormatException e) {

            }
        } else {
            cliWriter.println(translation.getTranslation(isSpecial ? "no_cards_to_play_on_overcharge" : "no_cards_to_play"));

            cliWriter.println();

            cliWriter.print(translation.getTranslation("remaining_cards"));
            cliWriter.println(":");

            for (Card remainingCard : player.getDeck()) {
                cliWriter.printf("\t%s\n", translation.getCardName(remainingCard));
            }

            cliWriter.println();

            cliWriter.println(translation.getTranslation("prompt_for_continue"));
            cliWriter.flush();

            try {
                cliReader.read();
            } catch (IOException e) {
            }
        }

        return Optional.empty();
    }

    @Override
    public void showFirstCard(Card card) {

    }

    @Override
    public void placeCard(Card card) {

    }

    @Override
    public void addCardToDeck(Player player, Card card) {

    }

    @Override
    public CardColor letPlayerSelectColor() {
        cliWriter.println();

        cliWriter.print(translation.getTranslation("colors_for_select"));
        cliWriter.println(":");

        var colors = CardColor.values();

        for (int i = 0; i < colors.length; i++) {
            cliWriter.printf("%d) %s\n", i, translation.getCardColorName(colors[i]));
        }

        cliWriter.println();

        cliWriter.println(translation.getTranslation("select_color"));
        cliWriter.print(": ");
        cliWriter.flush();

        try {
            var response = cliReader.readLine();

            return colors[Integer.parseInt(response)];
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void playerRetrievedOverChargedCards(Player player, Integer i) {

    }

    @Override
    public void gameOver(Players players, Integer round) {
        clearConsole();

        cliWriter.println(translation.getTranslation("game_over"));
        cliWriter.println();
        cliWriter.print(translation.getTranslation("winner_is"));

        var winners = players.getWinners();

        if(winners.size() > 1) {
            var i = 1;

            while(!winners.isEmpty()) {
                cliWriter.println(String.format("%d. %s", i++, winners.remove().getName()));
            }
        } else {
            cliWriter.println(winners.peek().getName());
        }

        cliWriter.flush();
    }

    @Override
    public void currentPlayer(Player player) {
        clearConsole();

        cliWriter.print(translation.getTranslation("player"));
        cliWriter.print(" ");
        cliWriter.print(player.getName());
        cliWriter.print(" ");
        cliWriter.println(translation.getTranslation("plays"));
        cliWriter.flush();
    }
}

package io.techniktom.card;

public enum CardColor implements Comparable<CardColor> {
    KULE, CERVENY, LISTY, ZALUDY
}

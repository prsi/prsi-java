package io.techniktom;

import io.techniktom.interfaces.CLIInterface;
import io.techniktom.player.Player;
import io.techniktom.translations.CzechTranslation;

import java.io.UnsupportedEncodingException;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException {
        var lang = new CzechTranslation();
        var gameInterface = new CLIInterface(lang);
        var players = Set.of(
            new Player("Tomáš1"),
            new Player("Tomáš2")
        );

        var game = new Game(players, gameInterface);

        game.prepareGame();
        game.startGame();
    }
}

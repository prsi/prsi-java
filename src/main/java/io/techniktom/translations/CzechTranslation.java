package io.techniktom.translations;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.techniktom.card.Card;
import io.techniktom.card.CardColor;
import io.techniktom.card.CardType;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CzechTranslation extends Translation {
    public CzechTranslation() {
        super("Czech", "Čeština");
    }

    private static String toValue(Card card) {
        return String.format(
            "%s %s",
            getNameForColor(card),
            getNameForType(card.getType())
        );
    }

    @Override
    protected Map<Card, String> cardTranslation() {
        return Card.Cards
            .getAllCards()
            .stream()
            .collect(Collectors.toUnmodifiableMap(Function.identity(), CzechTranslation::toValue));
    }

    @Override
    protected Map<CardColor, String> cardColorTranslation() {
        return Stream
            .of(CardColor.values())
            .collect(Collectors.toMap(Function.identity(), this::getNameForColor));
    }

    @Override
    protected Map<String, String> stringTranslation() {
        try {
            return new ObjectMapper().readValue(new File("translations/czech.json"), new TypeReference<>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new HashMap<>();
    }

    private static String getNameForType(CardType type) {
        return switch (type) {
            case SEVEN -> "sedma";
            case EIGHT -> "osma";
            case NINE -> "devítka";
            case X -> "desítka";
            case ESO -> "eso";
            case KRAL -> "král";
            case MENIC -> "měnič";
            case SVRSEK -> "svršek";
        };

    }

    private static String getNameForColor(Card card) {
        if(
            Objects.equals(card.getType(), CardType.ESO)
            || Objects.equals(card.getType(), CardType.MENIC)
            || Objects.equals(card.getType(), CardType.SVRSEK)
            || Objects.equals(card.getType(), CardType.KRAL)
        ) {
            return switch (card.getColor()) {
                case KULE -> "kuloví";
                case LISTY -> "zelený";
                case CERVENY -> "červený";
                case ZALUDY -> "žaludoví";
            };
        } else {
            return switch (card.getColor()) {
                case KULE -> "kulová";
                case LISTY -> "zelená";
                case CERVENY -> "červená";
                case ZALUDY -> "žaludobá";
            };
        }
    }

    private String getNameForColor(CardColor color) {
        return switch (color) {
            case KULE -> "kule";
            case LISTY -> "listy";
            case ZALUDY -> "žaludy";
            case CERVENY -> "červený";
        };
    }
}

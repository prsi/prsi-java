package io.techniktom.translations;

import java.util.Map;

public enum Langs {
    CZ, EN;

    public static Map<Langs, Translation> translations;

    static {
        translations = Map.of(
            CZ, new CzechTranslation(),
            EN, new EnglishTranslation()
        );
    }
}
